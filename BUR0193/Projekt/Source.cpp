#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;


struct ListItem
{
	int Value;
	ListItem* Prev;
	ListItem* Next;
};

struct List
{
	ListItem* Head;
	ListItem* Tail;
};

bool IsEmpty(const List& L)
{
	return L.Head == nullptr;
}

void InternalCreateSingleElementList(List& L, const int NewValue)
{
	L.Head = L.Tail = new ListItem;
	L.Head->Value = NewValue;
	L.Head->Prev = nullptr;
	L.Head->Next = nullptr;
}


void Append(List& L, const int NewValue)
{
	if (IsEmpty(L))
	{
		InternalCreateSingleElementList(L, NewValue);
	}
	else
	{
		ListItem* NewItem = new ListItem;
		NewItem->Value = NewValue;
		NewItem->Next = nullptr;
		L.Tail->Next = NewItem;
		NewItem->Prev = L.Tail;
		L.Tail = NewItem;
	}
}

void Init(List& L)
{
	L.Head = nullptr;
	L.Tail = nullptr;
}

void Init(List& L, const int InitValues[], const int N)
{
	Init(L);
	for (int i = 0; i < N; i++)
	{
		Append(L, InitValues[i]);
	}
}



void Init(List& L, const vector <int>& myVector, const int N)
{
	Init(L);
	for (int i = 0; i < N; i++)
	{
		Append(L, myVector.at(i));
	}
}



void ReportStructure(const List& L)
{
	cout << "L.Head: " << L.Head << endl;
	cout << "L.Tail: " << L.Tail << endl;
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		cout << "Item address: " << p << endl;
		cout << "Value: " << p->Value << endl;
		cout << "Prev: " << p->Prev << endl;
		cout << "Next: " << p->Next << endl;
		cout << endl;
	}
}

void Exchange(ListItem *p, ListItem *r) {
	int k = p->Value;
	p->Value = r->Value;
	r->Value = k;
}

void Sort(List &L) {
	ListItem *p = L.Head;
	ListItem *t = L.Tail;
	bool AnyExchange;

		do {
			AnyExchange = false;
			p = L.Head;
			for (p; p != t->Next; p = p->Next) {
				ListItem *r = p->Next;
				if (r != nullptr) {
					if ((p->Value) > (r->Value)) {
						Exchange(p, r);
						AnyExchange = true;
					}
				}

			}t = t->Prev;
			
		} while (AnyExchange);
		
} 


int main() {
	List X;
	const int N = 10;
	int pole[] = {8,7,6,5,1,2,3,4,9,0};
	
	
	/**srand(time(NULL));

	/**const int N = 10;
	int pole[N];
	for (int i = 0; i < N; i++) {
		pole[i] = rand() % 100;
	}
    
	/**vector <int> pole;
	for (int i = 0; i < 500; i++) {
		pole.push_back((rand() % 100));
	}
	
	
	const int N = pole.size();*/
	
	Init(X, pole, N);
	Sort(X);
	ReportStructure(X);
	system("pause");
	return 0;
}
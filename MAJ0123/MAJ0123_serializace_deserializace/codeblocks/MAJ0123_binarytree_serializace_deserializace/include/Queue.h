#ifndef QUEUE_H
#define QUEUE_H
#include "BinaryTree.h"

/**
 * @file Queue.h
 * @author Lukas Majoros
 * @date 20 Dubna 2018
 * @brief Hlavickovy soubor tridy Queue
 *
 * Hlavickovy soubor tridy Queue,
 */

 /****************************************************************************
 *   Prevazne ADSLibrary od Dvorskeho, misto intu BinaryTree Node, FIFO     *
 *   Pridavani do fronty je Enqueue a odebrani je Dequeue .                 *
 ****************************************************************************/



class Queue {
private:
	struct Item {
		Node * Node;
		Item * Next;
	};

	Item *Head, *Tail;
public:
	Queue();
	~Queue();

	void Enqueue(Node * node);
	Node * Dequeue();
	Node * Peek();
	bool IsEmpty();
	void Clear();
};


#endif // QUEUE_H
